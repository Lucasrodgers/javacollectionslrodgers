package rodgers.javacollection;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

public class CollectionTypes {


    public static void main(String[] args) throws InterruptedException {
        System.out.println("<----Queue---->\n");
        Queue names = new PriorityQueue();

        names.add("John");
        names.add("Rebeca");
        names.add("Timothy");
        names.add("Alexandra");
        names.add("Ian");
        names.add("Jenny");
        names.add("Jenny");

        System.out.println("The line at the Supermarket consists of " + names + "\n");

        //Prints John as the first person
        System.out.println("The first person in line is " + names.peek() + "\n");

        //This removes the first 3 people from the queue using FIFO
        for (int i=0; i<3; i++) {
            names.remove();
        }

        //Alexandra is not the first person in the queue
        System.out.println("After 10 minutes of waiting the first person in line is " + names.peek());



        System.out.println("\n\n\n<----Hash Map---->\n");
        HashMap<Integer, String> phoneBook = new HashMap<Integer, String>();

        phoneBook.put(456215, "F. Scott Fitzgerald");
        phoneBook.put(457842, "William Faulkner");
        phoneBook.put(987411, "Elvis Presley");
        phoneBook.put(468721, "Leo Tolstoy");
        phoneBook.put(748218, "Mark Twain");
        phoneBook.put(324621, "Peter Sellers");
        phoneBook.put(847213, "Sting");

        //Getting the value from a get call with the key.
        System.out.println("Tim got a missed call from \"468721\". Turns out it was " + phoneBook.get(468721));

        System.out.println("\nTim got a second missed call from \"847213\". Turns out it was " + phoneBook.get(847213)
                + ", Tim is his biggest fan!");



        System.out.println("\n\n\n<----Sorted Set---->\n");
        SortedSet<Integer> ages = new TreeSet<Integer>();
        ages.add(45);
        ages.add(52);
        ages.add(12);
        ages.add(26);
        ages.add(15);
        ages.add(31);
        ages.add(23);
        ages.add(18);

        //This shows that the set is sorted.
        System.out.println("The youngest person in the class is " + ages.first() + " years old\n");
        System.out.println("The oldest person in the class is " + ages.last() + " years old");

        System.out.println("\n\n\n<----Deque---->\n");
        Deque<String> onHold = new LinkedList<String>();
        onHold.add("Catherine");
        onHold.add("Rae");
        onHold.add("Meghan");
        onHold.add("Anna");
        onHold.add("Gareth");
        onHold.add("Phoebe");
        onHold.add("James");
        onHold.add("Carole");
        onHold.add("Dean");
        onHold.add("Sam");

        //Find out how many items are in the deque
        System.out.println("John is helping clients. There are currently " + onHold.size() +" people on hold...\n");

        //get an idea of who is in the front and back of the deque
        System.out.println("The next person on hold is " + onHold.peekFirst() + ". The last person on hold is "
                + onHold.peekLast());

        //this gets rid of the first 3 from the front
        for (int i=0; i<3; i++){
            onHold.removeFirst();
        }

        //find out the new first person
        Thread.sleep(3000);
        System.out.println("\nJohn took a long time helping clients...the next person on hold is " + onHold.peekFirst());

        //This shows that you can remove from the end of the deque as well
        for (int i=0; i<2; i++){
            onHold.removeLast();
        }

        //This shows the new last person
        System.out.println("\nThe last two people got tired of waiting and hung up. The new last person on hold is "
                + onHold.peekLast());

        //finds the current size, showing that they have been removed from the deque
        System.out.println("\nThere is now currently " + onHold.size() +" people on hold...");


        System.out.println("\n\n\n<----Navigable Set---->\n");

        NavigableSet<String> oldFriend = new ConcurrentSkipListSet<String>();
        oldFriend.add("Justin Mayer");
        oldFriend.add("Timothy Scoffer");
        oldFriend.add("Devon Millington");
        oldFriend.add("Skylar Weber");
        oldFriend.add("Asher Stone");
        oldFriend.add("Carolina Dejesus");
        oldFriend.add("Benito Reyes");
        oldFriend.add("Rudy Frank");
        oldFriend.add("Hayden Cannon");
        oldFriend.add("Janelle Pineda");

        //This shows that navigable set can find the closest match
        System.out.println("John is trying to search for old friends from school. " +
                "He remembers their first name but can't remember the correct spelling\n");

        System.out.println("He searches for Tim, and his search comes back:...   " + oldFriend.ceiling("Timothy"));
        Thread.sleep(2000);
        System.out.println("\nYes! That is him. Now he searched the name Carolena, his search comes back:...   "
                + oldFriend.ceiling("Carolina"));
        Thread.sleep(2000);
        System.out.println("\nOnce again, Yes! That's her. Now the last name he remembers is Skilar, his search comes back:...   "
                + oldFriend.ceiling("Skylar"));

    }
}